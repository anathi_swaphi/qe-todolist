const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: 'test/*test.js',
  output: 'output',
  helpers: {
    Puppeteer: {
      url: "http://localhost:",
      show: true,
      waitForAction: 300,
      pressKeyDelay: 300,
    },
  },
  include: {
    I: './steps_file.js',
    todoPages: './pages/todo_pages.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'qe-todolist',
  plugins: {
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    },
    allure: {
      enabled : true
    }
  }
}