const { I} = inject();

module.exports = {

  pauseTime : 5, 
  /* creating a new todo item */
  todoTextField : "//input[@id='newtodo']",
  todoSubmitButton : "//input[@id='new-submit']",

  /* updating a todo item */
  update(index){
    updateValue: "//input[@id='edittodo-"+this.index+"']";
    todoUpdateButton: "//input[@id='edit-submit-"+this.index+"']";
  },
  
  /* deleting a todo item */
  delete(todoDel){
    todoDelete : "//a[text()='✘'])['"+todoDel+"']"
  },
}
