Feature('todo');

const { assert } = require("chai");
const { update } = require("../pages/todo_pages");
/*
This test feature, navigates to the todo list site and creates,update and delete a todo list.
*/

Scenario('@creates 5 todo', ({ I, todoPages}) => {
    I.amOnPage('8080')
    for(i = 0; i < 5; i++){
        I.fillField(todoPages.todoTextField, "todo"+i)
        I.click(todoPages.todoSubmitButton)
        I.see("todo"+i) // validates that the todo item has been created
    }
    I.saveScreenshot("todoList.png", true)
});

Scenario('@update todo', ({ I, todoPages}) => {
    I.refreshPage()
    I.amOnPage('8080')
    for(i = 0; i < 5; i++){
        I.fillField("//input[@id='edittodo-"+i+"']", "updateTodo"+i) 
        I.click("//input[@id='edit-submit-"+i+"']") // validates that the todo item has been updated
    }
    I.saveScreenshot("updatedTodoList.png", true)

});
Scenario('@delete todo', ({ I, todoPages}) => {
    I.refreshPage()
    I.amOnPage('8080')
    for(i = 0; i <5; i++){
        I.waitForElement("(//a[text()='✘'])[1]", todoPages.pauseTime)
        I.click("(//a[text()='✘'])[1]")
        I.dontSee("updateTodo"+(i)) // validates that the todo item has been deleted
    }
    I.saveScreenshot("deletedTodoList.png", true)

});
