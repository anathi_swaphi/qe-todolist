**Test Plan**

**Contents**
                                                                                                            
1. **Contents**
2. **Project Details**
3. **Overview**
    - Test Links
    - Browsers and Devices
    - Performance
    - Automated Checks

4. **Test Objective**

5. **Scope**
    
    


6. **Out Of Scope**
    

7. **Project Risks**
    - Scope Creep
    - Skills Risk 
    - Cost Risks
    - Resources

8. **Test Deliverables**

**Project Details**

   
| PROJECT SIGNOFF | |
| ------ | ------ |
|Date/s| N/A|
| Created By | Anathi Swaphi |
| Reviewed By | Entersekt |  
| Sign Off By | Entersekt | 


**Overview**
 
 Design a Front End test plan, clearly showing all the neccessary steps to configure, test steps, and validate test cases.

 **Test Links**
   
| Environment |Link|
| ------ | ------ |
|Local| http://localhost:8080|
|Docker| http://localhost:8081|
|Kubernetes| http://a6201e0bb3ba143c0950e1b359b68678-1790942498.eu-west-1.elb.amazonaws.com/todo|

**Browsers and Devices**

| Browsers |Device|
| ------ | ------ |
|Chrome| MacOS _automated_|
|Firefox| MacOS _manual_|
|Safari| MacOS _manual_|

**Performance**

| Capture Method |Description|
| ------ | ------ |
|Browser Development Tools| Testing is conducted with the browsers dev tools on the network tab in order to identify API bottlenecks and load times.|

**Automated Checks**

| Framework |Description|
| ------ | ------ |
|codeceptjs with Puppeteer| CodeceptJS is a modern end to end testing framework with a special BDD-style syntax. The tests are written as a linear scenario of the user's action on a site.|
Once the project has been cloned, few commands should be up and running to see automation kicking off.

run : npm install to get all the dependencies 
the testing steps will be found in the test folder
to execute the test, run : npx codeceptjs run --steps --verbose
to generate the report run : allure serve output

link to codeceptjs : https://codecept.io/

**Test Objective**
- The test objective is to verify the functionality of the todo website, the project should focus on testing the operations; such as adding, updating and deleting a todo to guarantee that all these can work normally.

**Scope**
    
- Functional Suite Based Off Requirements     
    - Test Cases 

| Action | Expected |
| ------ | ------ |
| Launch the application | Todo page looads with the heading, text field and a submit button |
| Enter a new todo item and the click on Submit | Text is entered successfully and the todo item is added to the list, Update Value text field is visible with an Update button |
| Enter a todo update value on the Update Value text field and click on Update | Todo list is updated |
| Click on the "x" next to the newly added todo item | The recently updated todo item is deleted |

- Functional Suite Not Based on Requirements
    - Test Cases

| Action | Expected |
| ------ | ------ |
| Launch the application | Todo page looads with the heading, text field and a submit button |
| Enter two or more todo items with the same name | The application should not allow for todo item duplicates |
|Uodate one of the todo items added previously|The appliaction should not allow for the update

    - Web Automation
    
- Non-Functional Suite   
    - Performance(Desktop)
    
**Out Of Scope**
    
- API testing
- Backend Testing
- Automated Mobile Testing

        

**Project Risks**

     
- Scope Creep : It is inevitable that during the testing stages, as more issues are found, more work can emerge which may impact deadlines.    
- Skills Risk : With the framweork used and cloud tools used, upskilling might be neccessary to carry the tasks.    
- Cost Risks : Intergarating this on the CI pipeline might be costly given the choses aws infrastructure i.e Kubernetes
- Resources : Lack of team members due to umplanned,sick leave etc

**Test Deliverables**

- Before Testing Phase
    - Test Plan
    - Test cases
    - Automation Tools and Framework Design
- During The Testing
    - Test Tools
    - Test Data
    - Error Logs and Test Executions
- Test Closure
    - Test Report
    - Defect Report 
    - Release Notes






